# TlahtolAmoxtli

## NOTE: THIS PROJECT IS A WIP

## the Libre-source dictionary

TlahtolAmoxtli is the first dictionary of its kind, dynamically allowing to classify words with all their grammatical features across multiple languages.

## Database dicionary

The dictionary's database consists of 67 tables:

<details>

1.  ### word

    A `word` record serves the purpose of tying together a number of `meanings` equivalent across multiple `languages`.

1.  ### language

    A `language` record serves the purposes of:

    - classifying a `meaning` as belonging to a single `language`.
    - classifying a `source reference description` as belonging to a single `language`.
    - classifying an `etymology description` as belonging to a single `language`.

1.  ### spelling_system

    A `spelling system` record serves the purpose of classifying a `meaning`'s `spelling` as belonging to a single `spelling system`.

1.  ### meaning_type

    A `meaning type` record serves the purpose of classifying a `meaning` as either a _definition_ or a _translation_ or some other kind of `meaning`.

1.  ### priority

    A `priority` record serves the purpose of defining how important a `grammatical feature` or `grammeme` is for a given language.

1.  ### language_grammatical_feature_priority

    A `language grammatical feature priority` record serves the purpose of ranking the importance of a `grammatical feature` for a given `language`.

1.  ### grammatical_feature

    A `grammatical feature` record serves the purpose of storing one of the different kinds of features a `word` can participate in.

1.  ### language_grammeme_priority

    A `language grammeme priority` record serves the purpose of ranking the importance of a `grammeme` for a given `language`.

1.  ### grammeme

    A `grammeme` record serves the purpose of storing one of the different values that a single `grammatical feature` can contain.

1.  ### meaning

    A `meaning` record serves the purpose of storing one of the different values that a single `word` bears within a given `language`.

1.  ### meaning_grammeme

    A `meaning grammeme` record serves the purpose of linking one of the different `grammemes` a `meaning` contains.

1.  ### synonym

    A `synonym` record serves the purpose of linking one `meaning` as a `synonym` of another.

1.  ### spelling

    A `spelling` record serves the purpose of storing one of the different values that a single `meaning` can be written as, in a given `spelling system`.

1.  ### source_type

    A `source type` record serves the purpose of storing one of the different values indicating what kind a `source` belongs to.

1.  ### source

    A `source` record serves the purpose of storing a title where `meanings` and `etymologies` are taken from.

1.  ### author

    An `author` record serves the purpose of storing the full name of someone who authored a `source`.

1.  ### source_author

    A `source author` record serves the purpose of linking an `author` with a `source`.

1.  ### source_reference_type

    A `source reference type` record serves the purpose of storing one of the different values indicating what kind of reference is made to a `source`.

1.  ### source_reference

    A `source reference` record serves the purpose of tying together a number of `source reference descriptions` equivalent across multiple `languages`.

1.  ### source_reference_description

    A `source reference description` record serves the purpose of storing the value in a given language of a description referencing a source.

1.  ### meaning_source_reference

    A `meaning source reference` record serves the purpose of linking a `source reference` as belonging to a `meaning`.

1.  ### etymology

    An `etymology` record serves the purpose of tying together a number of `etymology descriptions` equivalent across multiple `languages`.

1.  ### etymology_description

    An `etymology description` record serves the purpose of storing the value of an etymological description in a given language.

1.  ### etymology_root

    An `etymology root` record serves the purpose of linking one `meaning` as an etymological `root` of another.

1.  ### etymology_source_reference

    An `etymology source reference` record serves the purpose of linking a `source reference` as belonging to an `etymology`.

1.  ### privilege

    A `privilege` record serves the purpose of storing one of the values indicating an action allowed by its granting.

1.  ### role

    A `role` record serves the purpose of storing one of the values indicating one of the titles a `user` may belong to.

1.  ### role_privilege

    A `role privilege` record serves the purpose of tying together one or more `privileges` as belonging to a `role`.

1.  ### user_status

    A `user status` record serves the purpose of storing one of the values indicating the status a `user` may be assigned with.

1.  ### user

    A `users` record serves the purpose of storing the identity of a member of the `tlahtolamoxtli` dictionary community.

1.  ### user_role

    A `user role` record serves the purpose of tying together one `role` to a `user` for a given `language`-space.

1.  ### proposal_status

    A `proposal status` record serves the purpose of storing one of the values indicating the status a `proposal` may be assigned with.

1.  ### proposal_subject_level

    A `proposal subject level` record serves the purpose of storing one of the values indicating the software-update level a `proposal subject` is assigned with.

1.  ### proposal_subject

    A `proposal subject` record serves the purpose of storing one of the values indicating what a `proposal` is subject of.

1.  ### proposal_type

    A `proposal type` record serves the purpose of storing one of the values indicating what a `proposal` is intending to do.

1.  ### proposal_rule

    A `proposal rule` record serves the purpose of storing what `proposal type` (can be done) to a `proposal subject`.

1.  ### proposal_update_type

    A `proposal update type` record serves the purpose of storing one of the values indicating what kind of update a `proposal update` belongs to.

1.  ### proposal

    A `proposal` record serves the purpose of storing the values of a `proposal`:

    - `parent` this `proposal` is a child of
    - `proposal subject` it belongs to
    - `proposal type` it intends to do
    - `proposal status` currently assigned
    - `title`
    - `description`

1.  ### proposer

    A `proposer` record serves the purpose of linking `proposal` to a `user` as its `proposer`.

1.  ### proposal_update_type

    A `proposal update type` record serves the purpose of storing one of the values indicating what kind of update a `proposal update` belongs to.

1.  ### proposal_update

    A `proposal update` record serves the purpose of storing an update effected by a `user` to a `proposal`.

1.  ### proposal_rule_user_role

    A `proposal rule user role` record serves the purpose of storing what kind of update is allowed by a `proposal_rule` and how many users of a given `role` are required by that rule to update a proposal.

1.  ### multimedia_type

    A `multiple type` record serves the purpose of storing one of the values a `mime type` belongs to.

1.  ### mime_type

    A `mime type` record serves the purpose of storing one of the values a `multimedia resource` belongs to.

1.  ### mime_type

    A `mime type` record serves the purpose of storing one of the values a `multimedia resource` belongs to.

1.  ### proposal_forum

    A `proposal forum` record serves the purpose of tying together all messages corresponding to a `proposal`.

1.  ### forum_message

    A `forum message` record serves the purpose of storing the value of a message by a `user`.

1.  ### multimedia_resource

    A `multimedia resource` record serves the purpose of storing the value of a link to a multimedia resource belonging to a `forum message`.

1.  ### language_proposal

    A `language proposal` record serves the purpose of linking a proposed `language` to its `proposal`.

1.  ### spelling_system_proposal

    A `spelling system proposal` record serves the purpose of linking a proposed `spelling system` to its `proposal`.

1.  ### meaning_type_proposal

    A `meaning type proposal` record serves the purpose of linking a proposed `meaning type` to its `proposal`.

1.  ### grammatical_feature_proposal

    A `grammatical feature proposal` record serves the purpose of linking a proposed `grammatical feature` to its `proposal`.

1.  ### grammeme_proposal

    A `grammeme proposal` record serves the purpose of linking a proposed `grammeme` to its `proposal`.

1.  ### source_proposal

    A `source proposal` record serves the purpose of linking a proposed `source` to its `proposal`.

1.  ### source_type_proposal

    A `source type proposal` record serves the purpose of linking a proposed `source type` to its `proposal`.

1.  ### author_proposal

    A `author proposal` record serves the purpose of linking an `author` to its `proposal`.

1.  ### source_author_proposal

    A `source author proposal` record serves the purpose of linking a proposed `source author` record to its `proposal`.

1.  ### proposal_subject_proposal

    A `proposal subject proposal` record serves the purpose of linking a proposed `proposal subject` record to its `proposal`.

1.  ### proposal_type_proposal

    A `proposal type proposal` record serves the purpose of linking a proposed `proposal type` record to its `proposal`.

1.  ### proposal_status_proposal

    A `proposal status proposal` record serves the purpose of linking a proposed `proposal status` record to its `proposal`.

1.  ### multimedia_type_proposal

    A `multimedia type proposal` record serves the purpose of linking a proposed `multimedia type` record to its `proposal`.

1.  ### mime_type_proposal

    A `mime type proposal` record serves the purpose of linking a proposed `mime type` record to its `proposal`.

1.  ### role_proposal

    A `role proposal` record serves the purpose of linking a proposed `role` record to its `proposal`.

1.  ### role_proposal

    A `role proposal` record serves the purpose of linking a proposed `role` record to its `proposal`.

1.  ### role_privilege_proposal

    A `role privilege proposal` record serves the purpose of linking a proposed `role privilege` record to its `proposal`.

1.  ### word_proposal

    A `word proposal` record serves the purpose of linking a proposed `word` record to its `proposal`.

1.  ### meaning_proposal

    A `meaning proposal` record serves the purpose of linking a proposed `word` record to its `proposal`.

1.  ### spelling_proposal

    A `spelling proposal` record serves the purpose of linking a proposed `spelling` record to its `proposal`.

1.  ### etymology_proposal

    A `etymology proposal` record serves the purpose of linking a proposed `etymology` record to its `proposal`.

1.  ### synonym_proposal

    A `synonym proposal` record serves the purpose of linking a proposed `synonym` record to its `proposal`.
    </details>
    <br>

## Proposal subject level

These records indicate the different levels of involvement (privilege) in the platform, i.e. how deep into the code proposal's changes affect the platform, is it a?:

- DB DDL change
- Back end source code change
- DB DML change

## Examples

Following are examples of the entities' relations:

- **_to write_** and **_escribir_** are two `meanings` belonging to the same `word` since they convey the same `meaning` but in different `languages`, i.e. **English** and **Spanish** respectively.

- **_to burn_** and **_to break_** convey different `meanings`, thus each of this `meanings` belongs to a different `word` but in the same `language`.

- **_Beograd_** and **_Београд_** convey the same `meaning` in the same `language` but in different `spelling systems`, i.e. Latin and Cyrillic respectively.

- A **_grammeme_** is the name given to any value a `grammatical category` may hold.

- A **_grammatical category_** is a `grammatical feature` and its `grammemes` may be any one of **_noun_**, **_verb_**, **_adjective_**, etc. Another `grammatical feature` is **_number_** and its `grammemes` may be one of **_singular_**, **_dual_**, **_plural_**, etc.

1. ### Spellings

   <details>
   <details>

   meaning_type
   | id | name |
   | - | - |
   | 1 | Plain |
   | 2 | Definition |
   <br>

   language
   | id | name |
   | - | - |
   | 1 | English |
   | 2 | Serbian |
   | 3 | Spanish |
   <br>

   spelling_system
   | id | name |
   | - | - |
   | 1 | Latin |
   | 2 | Cyrillic |
   <br>

   word
   | id |
   | - |
   | 1 |
   <br>

   meaning
   | id | meaning_type_id | word_id | language_id |
   | - | - | - | - |
   | 1 | 1 | 1 | 1 |
   | 2 | 2 | 1 | 1 |
   | 3 | 1 | 1 | 2 |
   | 4 | 1 | 1 | 3 |
   <br>

   spelling
   | id | meaning_id | spelling_system_id | spelling |
   | - | - | - | - |
   | 1 | 1 | 1 | Belgrade |
   | 2 | 2 | 1 | Capital of Serbia |
   | 3 | 3 | 1 | Beograd |
   | 4 | 3 | 2 | Београд |
   | 5 | 4 | 1 | Belgrado |
   <br>
   </details>

   summarizing:
   | word_id | language | meaning_id | meaning_type | spelling_system | spelling |
   | - | - | - | - | - | - |
   | 1 | English | 1 | Plain | Latin | Belgrade |
   | 1 | English | 2 | Definition | Latin | Capital of Serbia |
   | 1 | Serbian | 2 | Plain | Latin | Beograd |
   | 1 | Serbian | 2 | Plain | Cyrillic | Београд |
   | 1 | Spanish | 3 | Plain | Latin | Belgrado |
   </details>

1. ### Plain meanings and definitions

   <details>
   <details>

   meaning_type
   | id | name |
   | - | - |
   | 1 | Plain |
   | 2 | Definition |
   <br>

   language
   | id | name |
   | - | - |
   | 1 | English |
   | 2 | Serbian |
   | 3 | Spanish |
   <br>

   spelling_system
   | id | name |
   | - | - |
   | 1 | Latin |
   | 2 | Cyrillic |
   <br>

   word
   | id |
   | - |
   | 1 |
   <br>

   meaning
   | id | meaning_type_id | word_id | language_id |
   | - | - | - | - |
   | 1 | 1 | 1 | 1 |
   | 2 | 1 | 1 | 1 |
   | 3 | 2 | 1 | 1 |
   | 4 | 1 | 1 | 2 |
   | 5 | 1 | 1 | 2 |
   | 6 | 1 | 1 | 3 |
   | 7 | 1 | 1 | 3 |
   <br>

   spelling
   | id | meaning_id | spelling_system_id | spelling |
   | - | - | - | - |
   | 1 | 1 | 1 | apple |
   | 2 | 2 | 1 | apples |
   | 3 | 3 | 1 | fruit red or green on the outside and white inside |
   | 4 | 4 | 1 | jabuka |
   | 5 | 5 | 1 | jabuke |
   | 6 | 4 | 2 | јабука |
   | 7 | 5 | 2 | јабуке |
   | 8 | 6 | 1 | manzana |
   | 9 | 7 | 1 | manzanas |
   <br>

   grammatical_feature
   | id | name |
   | - | - |
   | 1 | Number |
   | 2 | Gender |
   <br>

   grammeme
   | id | feature_id | value |
   | - | - | - |
   | 1 | 1 | Singular |
   | 2 | 1 | Plural |
   | 3 | 2 | Neuter |
   | 4 | 2 | Male |
   | 5 | 2 | Female |
   <br>

   meaning_grammeme
   | meaning_id | grammeme_id |
   | - | - |
   | 1 | 1 |
   | 1 | 3 |
   | 2 | 2 |
   | 2 | 3 |
   | 4 | 1 |
   | 4 | 5 |
   | 5 | 2 |
   | 5 | 5 |
   | 6 | 1 |
   | 6 | 5 |
   | 7 | 2 |
   | 7 | 5 |
   <br>
   </details>

   summarizing:
   | word_id | language | meaning_id | meaning_type | spelling_system | spelling | Number | Gender |
   | - | - | - | - | - | - | - | - |
   | 1 | English | 1 | Plain | Latin | apple | Singlar | Neuter |
   | 1 | English | 2 | Plain | Latin | apples | Plural | Neuter |
   | 1 | English | 3 | Definition | Latin | fruit red or green on the outside and white inside |
   | 1 | Serbian | 4 | Plain | Latin | jabuka | Singlar | Female |
   | 1 | Serbian | 4 | Plain | Cyrillic | јабука | Singlar | Female |
   | 1 | Serbian | 5 | Plain | Latin | jabuke | Plural | Female |
   | 1 | Serbian | 5 | Plain | Cyrillic | јабуке | Plural | Female |
   | 1 | Spanish | 6 | Plain | Latin | manzana | Singlar | Female |
   | 1 | Spanish | 7 | Plain | Latin | manzanas | Plural | Female |
   </details>

1. ### Synonyms

   <details>
   <details>

   language
   | id | name |
   | - | - |
   | 1 | English |
   | 3 | Spanish |
   <br>

   word
   | id |
   | - |
   | 1 |
   | 2 |
   | 3 |
   <br>

   meaning
   | id | word_id | language_id |
   | - | - | - |
   | 1 | 1 | 1 |
   | 2 | 2 | 1 |
   | 3 | 3 | 1 |
   | 4 | 1 | 3 |
   | 5 | 2 | 3 |
   | 6 | 3 | 3 |
   <br>

   spelling
   | id | meaning_id | spelling |
   | - | - | - |
   | 1 | 1 | break |
   | 2 | 2 | shatter |
   | 3 | 3 | crack |
   | 4 | 4 | romper |
   | 5 | 5 | destrozar |
   | 6 | 6 | agrietar |
   <br>

   synonym
   | meaning_id | synonym_id |
   | - | - |
   | 1 | 2 |
   | 2 | 3 |
   | 3 | 1 |
   | 4 | 5 |
   | 5 | 6 |
   | 6 | 4 |
   <br>
   </details>

   summarizing:
   | word_id | language | meaning_id | spelling | synonyms |
   | - | - | - | - | - |
   | 1 | English | 1 | break | shatter, crack |
   | 2 | English | 2 | shatter | break, crack |
   | 3 | English | 3 | crack | break, shatter |
   | 1 | Spanish | 4 | romper | destrozar, agrietar |
   | 2 | Spanish | 5 | destrozar | romper, agrietar |
   | 3 | Spanish | 6 | agrietar | romper, destrozar |
   </details>

1. ### Etymologies

   <details>
   <details>

   language
   | id | name |
   | - | - |
   | 1 | Mexika |
   | 2 | English |
   | 3 | Spanish |
   <br>

   word
   | id |
   | - |
   | 1 |
   | 2 |
   | 3 |
   <br>

   meaning
   | id | word_id | language_id |
   | - | - | - |
   | 1 | 1 | 1 |
   | 2 | 2 | 1 |
   | 3 | 3 | 1 |
   | 4 | 1 | 2 |
   | 5 | 2 | 2 |
   | 6 | 3 | 2 |
   | 7 | 1 | 3 |
   | 8 | 2 | 3 |
   | 9 | 3 | 3 |
   <br>

   spelling
   | id | meaning_id | spelling |
   | - | - | - |
   | 1 | 1 | awakatl |
   | 2 | 2 | tamalli |
   | 3 | 3 | koyotl |
   | 4 | 4 | avocado |
   | 5 | 5 | tamale |
   | 6 | 6 | coyote |
   | 7 | 7 | aguacate |
   | 8 | 8 | tamal |
   | 9 | 9 | coyote |
   <br>

   etymology
   | id | meaning_id |
   | - | - |
   | 1 | 7 |
   | 2 | 8 |
   | 3 | 9 |
   | 4 | 4 |
   | 5 | 5 |
   | 6 | 6 |
   <br>

   etymology_root
   | etymology_id | root_meaning_id |
   | - | - |
   | 1 | 1 |
   | 2 | 2 |
   | 3 | 3 |
   | 4 | 7 |
   | 5 | 8 |
   | 6 | 9 |
   <br>

   etymology\*description
   | etymology\*id | language\*id | description |
   | - | - | - |
   | 1 | 3: Spanish | del náhuatl \*\*\_awakatl**\* |
   | 1 | 2: English | from nahuatl **\_awakatl**\* |
   | 2 | 2: English | from nahuatl **\_tamalli**\* |
   | 3 | 2: English | from nahuatl **_koyotl_** |
   | 4 | 2: English | from spanish **_aguacate_** |
   | 5 | 2: English | from spanish **_tamal_** |
   | 6 | 2: English | from spanish **_coyote_\*\* |
   <br>
   </details>

   summarizing:
   | word_id | language | meaning_id | spelling | roots |
   | - | - | - | - | - |
   | 1 | Mexika | 1 | awakatl |
   | 2 | Mexika | 2 | tamalli |
   | 3 | Mexika | 3 | koyotl |
   | 4 | English | 4 | avocado | 7: aguacate |
   | 5 | English | 5 | tamale | 8: tamal |
   | 6 | English | 6 | coyote | 9: coyote |
   | 7 | Spanish | 7 | aguacate | 1: awakatl |
   | 8 | Spanish | 8 | tamal | 2: tamalli |
   | 9 | Spanish | 9 | coyote | 3: koyotl |
   </details>

1. ### Authors, Sources and References

   <details>

   source_type
   | id | name |
   | - | - |
   | 1 | Dictionary |
   | 2 | Grammar Book |
   <br>

   source
   | id | title | type_id |
   | - | - | - |
   | 1 | Diccionario de la lengua Mexicana | 1: Dictionary |
   | 2 | Gramario de la lengua Mexicana | 2: Grammar Book |
   <br>

   author
   | id | name | family_name |
   | - | - | - |
   | 1 | Ignacio Tonatiuh | Valdes Lopez |
   | 2 | Mario Cuauhtemoc | Vergara Gonzalez |
   | 3 | Luis Alejandro | Gimenez Tezonco |
   <br>

   source_author
   | source_id | author_id |
   | - | - |
   | 1: Diccionario de la lengua Mexicana | 1: Ignacio Tonatiuh |
   | 1: Diccionario de la lengua Mexicana | 2: Mario Cuauhtemoc |
   | 2: Diccionario de la lengua Mexicana | 3: Luis Alejandro |
   <br>

   language
   | id | name |
   | - | - |
   | 1 | English |
   | 3 | Spanish |
   <br>

   source_reference_type
   | id | name |
   | - | - |
   | 1 | Literary |
   | 2 | Digital |
   <br>

   word
   | id |
   | - |
   | 1 |
   | 2 |
   | 3 |
   <br>

   meaning
   | id | word_id |
   | - | - |
   | 1 | 1 |
   | 2 | 2 |
   | 3 | 3 |
   <br>

   spelling
   | id | meaning_id | spelling |
   | - | - | - |
   | 1 | 1 | masatl |
   | 2 | 2 | texokotl |
   | 3 | 3 | masatexokotl |
   <br>

   etymology
   | id | meaning_id |
   | - | - |
   | 1 | 3: masatexokotl |
   <br>

   etymology_root
   | etymology_id | root_meaning_id |
   | - | - |
   | 1 | 1: masatl |
   | 1 | 2: texokotl |
   <br>

   source_reference
   | id | type_id | source_id |
   | - | - | - |
   | 1 | 1: Literary | 2: Gramario de la lengua Mexicana |
   | 2 | 1: Literary | 1: Diccionario de la lengua Mexicana |
   <br>

   etymology_source_reference
   | reference_id | etymology_id |
   | - | - |
   | 2 | 1 |
   <br>

   meaning_source_reference
   | reference_id | meaning_id |
   | - | - |
   | 1 | 2 |
   <br>

   source_reference_description
   | id | reference_id | language_id | description |
   | - | - | - | - |
   | 1 | 1 | 3: Spanish | Página 23, ejercicio 8 |
   | 2 | 1 | 1: English | Page 23, excercise 8 |
   | 3 | 2 | 3: Spanish | Página 67 |
   | 4 | 2 | 1: English | Page 67 |
   <br>

   </details>

1. ### Proposals, Proposal Levels, Proposal Rules, Privileges and User Roles

   <details open>
   <details open>

   privilege
   | id | name |
   | - | - |
   | 1 | Propose a word |
   | 2 | Approve a word |
   | 3 | Reject a word |
   | 4 | Propose a meaning |
   | 5 | Approve a meaning |
   | 6 | Reject a meaning |
   | 7 | Propose a spelling |
   | 8 | Approve a spelling |
   | 9 | Reject a spelling |
   <br>

   role
   | id | name |
   | - | - |
   | 1 | Proposer |
   | 2 | Curator |
   <br>

   role_privilege
   | role_id | privilege_id |
   | - | - |
   | 1: Proposer | 1: Propose a word |
   | 1: Proposer | 4: Propose a meaning |
   | 1: Proposer | 7: Propose a spelling |
   | 2: Curator | 2: Approve a word |
   | 2: Curator | 3: Reject a word |
   | 2: Curator | 5: Approve a meaning |
   | 2: Curator | 6: Reject a meaning |
   | 2: Curator | 8: Approve a spelling |
   | 2: Curator | 9: Reject a spelling |
   <br>

   user_status
   | id | name |
   | - | - |
   | 1 | Pending email confirmation |
   | 2 | Account activated |
   | 3 | Suspended |
   <br>
   
   user
   | id | status_id | username |
   | - | - | - |
   | 1 | 2 | tlenexkoyotl |
   | 2 | 2 | proposer1 |
   | 3 | 2 | curator1 |
   | 4 | 2 | curator2 |
   | 5 | 2 | curator3 |
   <br>

   user_role
   | user_id | role_id |
   | - | - |
   | 1: tlenexkoyotl | 1: Proposer |
   | 1: tlenexkoyotl | 2: Curator |
   | 2: proposer1 | 1: Proposer |
   | 3: curator1 | 1: Proposer |
   | 3: curator1 | 2: Curator |
   | 4: curator2 | 1: Proposer |
   | 4: curator2 | 2: Curator |
   | 5: curator3 | 1: Proposer |
   | 5: curator3 | 2: Curator |
   <br>

   language
   | id | name |
   | - | - |
   | 1 | English |
   | 2 | Serbian |
   | 3 | Spanish |
   <br>

   spelling_system
   | id | name |
   | - | - |
   | 1 | Latin |
   | 2 | Cyrillic |
   <br>

   word
   | id |
   | - |
   | 1 |
   <br>

   meaning
   | id | meaning_type_id | word_id | language_id |
   | - | - | - | - |
   | 1 | 1 | 1 | 1 |
   | 2 | 1 | 1 | 1 |
   | 3 | 2 | 1 | 1 |
   | 4 | 1 | 1 | 2 |
   | 5 | 1 | 1 | 2 |
   | 6 | 1 | 1 | 3 |
   | 7 | 1 | 1 | 3 |
   <br>

   spelling
   | id | meaning_id | spelling_system_id | spelling |
   | - | - | - | - |
   | 1 | 1 | 1 | apple |
   | 2 | 2 | 1 | apples |
   | 3 | 3 | 1 | fruit red or green on the outside and white inside |
   | 4 | 4 | 1 | jabuka |
   | 5 | 5 | 1 | jabuke |
   | 6 | 4 | 2 | јабука |
   | 7 | 5 | 2 | јабуке |
   | 8 | 6 | 1 | manzana |
   | 9 | 7 | 1 | manzanas |
   <br>

   grammatical_feature
   | id | name |
   | - | - |
   | 1 | Number |
   | 2 | Gender |
   <br>

   grammeme
   | id | feature_id | value |
   | - | - | - |
   | 1 | 1 | Singular |
   | 2 | 1 | Plural |
   | 3 | 2 | Neuter |
   | 4 | 2 | Male |
   | 5 | 2 | Female |
   <br>

   meaning_grammeme
   | meaning_id | grammeme_id |
   | - | - |
   | 1 | 1 |
   | 1 | 3 |
   | 2 | 2 |
   | 2 | 3 |
   | 4 | 1 |
   | 4 | 5 |
   | 5 | 2 |
   | 5 | 5 |
   | 6 | 1 |
   | 6 | 5 |
   | 7 | 2 |
   | 7 | 5 |
   <br>
    </details>

   summarizing:
   | word_id | language | meaning_id | meaning_type | spelling_system | spelling | Number | Gender |
   | - | - | - | - | - | - | - | - |
   | 1 | English | 1 | Plain | Latin | apple | Singlar | Neuter |
   | 1 | English | 2 | Plain | Latin | apples | Plural | Neuter |
   | 1 | English | 3 | Definition | Latin | fruit red or green on the outside and white inside |
   | 1 | Serbian | 4 | Plain | Latin | jabuka | Singlar | Female |
   | 1 | Serbian | 4 | Plain | Cyrillic | јабука | Singlar | Female |
   | 1 | Serbian | 5 | Plain | Latin | jabuke | Plural | Female |
   | 1 | Serbian | 5 | Plain | Cyrillic | јабуке | Plural | Female |
   | 1 | Spanish | 6 | Plain | Latin | manzana | Singlar | Female |
   | 1 | Spanish | 7 | Plain | Latin | manzanas | Plural | Female |
   </details>

## License pending
